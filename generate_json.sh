#!/bin/sh


if [ ! -d openwrt ]; then
  git clone https://git.openwrt.org/openwrt/openwrt.git/
  cd openwrt
  make defconfig
else
  cd openwrt
  git pull
  make defconfig
fi

git grep "DEVICE_TITLE" | grep -Po ":=\s?[^\\\$]*" | grep -Po "[^:=\s?][^\\\$]*" | sort | sed 's/$/"/' | sed 's/^/"/' | tr '\n' , | sed 's/\(.*\),/\1]/' | sed '1s/^/[/' > ../devices1.json

cd ..

cat devices1.json | python3 python-parser.py

rm devices1.json

