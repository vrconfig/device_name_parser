#!/usr/bin/env python3

import sys 	# reading stdin
import json # parsing json
import re	# using regex

string = sys.stdin.read()
parsed_json = json.loads(string)


devices = None
with open ("openwrt/tmp/.config-target.in", "r") as myfile:
    devices=myfile.readlines()

output_json = '{'

last_item = None

for device_title in parsed_json:
	if device_title == "Generic":
		continue
	s = 'bool "'+device_title+'"'
	for item in devices:
		if re.search(s, item):			
			if re.match('config ', last_item):
				device_string = last_item.strip().split(" ")[1]
				for item2 in devices:
					if re.search("if " + device_string, item2):
						final_string = item2.strip().split(" ")[1]
						print('"'+device_title+'"'+': '+final_string)
						output_json += '"'+device_title+'"'+': '+final_string
						output_json += ','				
		last_item = item

output_json = output_json[:-1] # remove last character which is a comma
output_json += '}'

with open("devices.json", "w") as text_file:
    print(output_json, file=text_file)


