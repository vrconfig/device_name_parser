# Openwrt Device Name Parser

Parses the openwrt git repository and creates a JSON-file with all supported router models and their corresponding DEVICE string

## Usage

Just run the generate_json.sh script to create a new up-to-date JSON file.